 $('#loginform').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      name: {
          message: 'The username is not valid',
          validators: {
              notEmpty: {
                  message: 'The username is required'
              },
              stringLength: {
                  min: 6,
                  max: 30,
                  message: 'The username must be more than 6 and less than 30 characters long'
              },
              regexp: {
                  regexp: /^[a-zA-Z0-9_\.]+$/,
                  message: 'The username can only consist of alphabetical, number, dot and underscore'
              }
          }
      },
      password: {
          message: 'The password is not valid',
          validators: {
            notEmpty: {
                message: 'The password is required'
            },
            stringLength: {
                min: 4,
                max: 30,
                message: 'The username must be more than 6 and less than 30 characters long'
            }
          }
      }
    }
});