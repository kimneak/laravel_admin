<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
    <link href="{{ asset('vendor/dist/css/formValidation.min.css') }}" type="text/css">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('app/css/morris.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('app/css/sb-admin.css') }}" rel="stylesheet" type="text/css" >
  </head>
  <body >
    
      @yield('sidebar')
      @yield('sidebar_content')
 
      @yield('content')
      

  </body>
</html>
<script type="text/javascript" src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/dist/js/formValidation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/dist/js/framework/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jquery/app.validate.js') }}"></script>
<script type="text/javascript">
  $('#flash-overlay-modal').modal();
  $('.alert').delay(5000).hide(0);
</script>

