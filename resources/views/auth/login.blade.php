@extends('master')
@section('content')
@include('flash::message')
<div class="col-md-4 col-md-offset-4 vertical-center-row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Login</h3>
    </div>
    <div class="panel-body">
      <form id="loginform" method="POST" action="/auth/login">
        {!! csrf_field() !!}
        @if (count($errors) > 0)
          <div class="form-group">
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li style="list-style-type:none;"><span class="glyphicon glyphicon-warning-sign"></span>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <div class="form-group">
          <label for="name" class="control-label">User name</label>
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
            <input type="text" name="name" class="form-control" placeholder="User name" id="name">
          </div>
        </div>

        <div class="form-group">
          <label for="password" class="control-label">Password</label>
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
          </div>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox"> Remember me
          </label>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
        <button type="reset" class="btn btn-default">Cancel</button>
      </form>
    </div>
  </div>
</div>
@stop